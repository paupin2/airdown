airdown creates full offline HTML copy of Airtable bases, with attachments.
It's a simple and free (although much more limited) alternative to services such as
[On2Air](https://on2air.com/backups/) or [ProBackup](https://probackup.io/backup/airtable/).

![Airdown](airdown.svg)

First you'll need an Airtable personal address token,
which you can create/fetch from [here](https://airtable.com/create/tokens).
The token needs the scopes `data.records:read`, `data.recordComments:read` and `schema.bases:read`.

To create a full backup, do: `airdown [-dir=backup.NOW] [-tokenfn=token.txt] backup`.

If the output directory contains the string "NOW", it will be replaced with the current timestamp.

You can either save the token in a file, and point to it via `-tokenfn`, specify it directly via `-token`
or put it in the `AIRTABLE_TOKEN` environment variable.

Airdown will create a directory structure like this: 

```
backup-dir
 | `- app1
 | | `- tbl1
 | | | `- index.html # list of records in table, links to attachments
 | | | `- file1.pdf
 | | | `- ...
 | | `- index.html # list of tables, links to each one
 | | `- ...
 | `- app2
 | `- ...
 `- main.css
 `- index.html # list of bases, links to each one
```

You can find pre-build binaries [here](https://gitlab.com/paupin2/airdown/-/releases),
but they are not signed, so you might have to work around Windows/macOS's warnings.