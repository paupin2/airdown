package main

import (
	"io"
	"net/http"
	"os"
	"sync"

	"github.com/rs/zerolog/log"
)

type file struct{ remote, local string }
type downloader struct {
	queue chan file
	sync.WaitGroup
}

const (
	downloadQueueSize = 100
)

// newDownloader creates a new downloader, with goroutines to download files in "d.queue"
func newDownloader() *downloader {
	dl := downloader{queue: make(chan file, downloadQueueSize)}
	if count := *downloadThreads; count > 0 {
		dl.Add(count)
		for id := 1; id <= count; id++ {
			go func(id int) {
				for f := range dl.queue {
					f.download(id)
				}
				dl.Done()
			}(id)
		}
	}

	return &dl
}

func (df *file) download(thread int) {
	log := log.With().Int("thread", thread).Str("local", df.local).Str("url", df.remote).Logger()

	// create blank file
	f, err := os.Create(df.local)
	if err != nil {
		log.Fatal().Err(err).Msg("creating file")
	}
	defer f.Close()

	resp, err := http.Get(df.remote)
	if err != nil {
		log.Fatal().Err(err).Msg("getting file")
	}
	defer resp.Body.Close()

	// Writer the body to file
	_, err = io.Copy(f, resp.Body)
	if err != nil {
		log.Fatal().Err(err).Msg("writing file")
	}
	log.Info().Msg("downloaded")
}

func (d *downloader) push(l, r string) {
	log.Info().Str("local", l).Str("url", r).Msg("queued")
	d.queue <- file{remote: r, local: l}
}
