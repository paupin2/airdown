package main

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func backup() {
	// make base dir
	bases := listBases()
	dirName := strings.Replace(*outDir, "NOW", time.Now().Format("20060102.150405"), 1)
	mkdirp(dirName)

	// save main.css
	if err := os.WriteFile(path.Join(dirName, "main.css"), []byte(cssContent), 0644); err != nil {
		log.Fatal().Str("filename", path.Join(dirName, "main.css")).Err(err).Msg("saving")
	}

	// create index at out/index.html
	findex := openFile("All bases", "main.css", path.Join(dirName, indexhtml))
	findex.startTable("Base", "Tables")

	// list each base
	for _, b := range bases {
		// list tables for base, add row to index
		tables := b.tables()

		// create base index at out/<baseid>/index.html
		mkdirp(path.Join(dirName, b.Id))
		basefile := openFile(b.Name, "../main.css", path.Join(dirName, b.Id, indexhtml))
		basefile.write(`<p>Base <b>%s</b> has %d tables (back <a href="../index.html">to index</a>)</p>`, b.Name, len(tables))
		basefile.startTable("Table", "Records")

		findex.tablerow(
			fmt.Sprintf(`<a href="%s">%s</a>`, path.Join(b.Id, indexhtml), b.Name),
			fmt.Sprint(len(tables)),
		)

		// list records for each table
		mkdirp(dirName, b.Id)
		for _, t := range tables {
			// create table file at out/<baseid>/<tableid>/index.html
			mkdirp(dirName, b.Id, t.Id)
			tablefile := openFile(t.Name, "../../main.css", path.Join(dirName, b.Id, t.Id, indexhtml))
			tablefile.write(`<p>Table %s &gt; <b>%s</b> (back <a href="../index.html">to %s</a> or <a href="../../index.html">to index</a>)</p>`,
				b.Name,
				t.Name,
				b.Name,
			)

			// add table records
			recordCount := tablefile.loadTable(b.Id, t)
			tablefile.close()

			// add table to base list
			basefile.tablerow(
				fmt.Sprintf(`<a href="%s">%s</a>`, path.Join(t.Id, indexhtml), t.Name),
				fmt.Sprint(recordCount),
			)
		}
		basefile.endTable()
		basefile.close()
	}
	findex.endTable()
	findex.close()
}

type htmlFile struct {
	f          *os.File
	castFailed bool
	dir        string
	filename   string
}

func (hf *htmlFile) log() *zerolog.Logger {
	l := log.With().Str("filename", filepath.Join(hf.dir, hf.filename)).Logger()
	return &l
}

func (hf *htmlFile) write(format string, a ...any) {
	if hf.f == nil {
		panic("isnil")
	}
	if _, err := fmt.Fprintf(hf.f, format, a...); err != nil {
		hf.log().Fatal().Err(err).Msg("writing to file")
	}
}

func openFile(title, stylesheet, fn string) *htmlFile {
	f, err := os.Create(fn)
	hf := htmlFile{f: f}
	hf.dir, hf.filename = filepath.Split(fn)
	log := hf.log()
	if err != nil {
		log.Fatal().Err(err).Msg("opening")
	}

	hf.write(`<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="%s">
<title>%s</title>
</head>
<body>`, stylesheet, title)
	log.Info().Msg("opened")
	return &hf
}

func (hf *htmlFile) close() {
	if hf.f == nil {
		panic("already closed")
	}
	hf.write(`</body></html>`)
	if err := hf.f.Close(); err != nil {
		hf.log().Fatal().Err(err).Msg("closing file")
	}
	hf.f = nil
}

func (hf *htmlFile) tablerow(cells ...string) {
	hf.write(`<tr>`)
	for _, h := range cells {
		hf.write(`<td>%s</td>`, h)
	}
	hf.write(`</tr>`)
}

func (hf *htmlFile) startTable(headers ...string) {
	hf.write(`<table><tbody><tr>`)
	for _, h := range headers {
		hf.write(`<th>%s</th>`, h)
	}
	hf.write(`</tr>`)
}

func (hf *htmlFile) endTable() {
	defer hf.write(`</tbody></table>`)
}
