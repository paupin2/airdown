package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
)

var (
	lastRequestSent = time.Now().Add(-time.Hour)
	personalToken   string
)

// airtable does a request to the specified path in airtable.
// It encodes and decodes arguments, inputs and output, and deals with
// authentication and rate limiting.
func airtable(method, path string, args url.Values, in, out any) {
	parsed, err := url.Parse(*baseURL + "/" + strings.TrimPrefix(path, "/"))
	if err != nil {
		log.Fatal().Err(err).Msg("parsing url")
	}
	if len(args) > 0 {
		parsed.RawQuery = args.Encode()
	}

	u := parsed.String()
	log := log.With().Str("method", method).Str("url", u).Logger()

	// encode input
	var requestBody io.Reader
	if in != nil {
		var buf bytes.Buffer
		enc := json.NewEncoder(&buf)
		if err := enc.Encode(in); err != nil {
			log.Fatal().Err(err).Msg("encoding")
		}

		if *logRequests {
			log.Info().Msgf("request body: %s", buf.String())
		}

		requestBody = &buf
	}

	// create request, add headers
	req, err := http.NewRequest(method, u, requestBody)
	if err != nil {
		log.Fatal().Err(err).Msg("creating request")
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", personalToken))
	if in != nil {
		req.Header.Set("Content-Type", "application/json")
	}

	// wait until we can do the next request
	if now, next := time.Now(), lastRequestSent.Add(*delay); next.After(now) {
		time.Sleep(next.Sub(now))
	}

	// do request
	if *logHeaders {
		for k, hs := range req.Header {
			for _, v := range hs {
				log.Info().Str("key", k).Str("val", v).Msg("request header")
			}
		}
	}
	resp, err := http.DefaultClient.Do(req)
	log = log.With().Str("status", resp.Status).Logger()
	if err != nil {
		log.Fatal().Err(err).Msg("sending request")
	}

	// dump response headers
	if *logHeaders {
		for k, hs := range resp.Header {
			for _, v := range hs {
				log.Info().Str("key", k).Str("val", v).Msg("response header")
			}
		}
	}

	if resp.StatusCode < 100 || resp.StatusCode >= 400 {
		log.Fatal().Err(err).Msg("bad status")
	}

	// decode response body
	if resp.Body != nil && out != nil {
		buf, err := io.ReadAll(resp.Body)
		resp.Body.Close()
		if err == nil {
			err = json.Unmarshal(buf, &out)
		}
		if err != nil {
			log.Fatal().Err(err).Msg("decoding")
		}

		if *logResponses {
			log.Info().Msgf("response body: %s", string(buf))
		}
	}

	log.Info().Msg("request")
}
