package main

import (
	"fmt"
	"net/url"
)

type base struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

func listBases() []base {
	var out []base
	args := url.Values{}
	for {
		var reply struct {
			Bases  []base `json:"bases"`
			Offset string `json:"offset"`
		}

		// see: https://airtable.com/developers/web/api/list-bases
		airtable("GET", "/meta/bases", args, nil, &reply)

		out = append(out, reply.Bases...)
		if reply.Offset == "" {
			// no more pages
			break
		}
		// fetch next page using this offset
		args.Set("offset", reply.Offset)
	}
	return out
}

type field struct {
	Id          string         `json:"id"`
	Type        string         `json:"type"`
	Name        string         `json:"name"`
	Description string         `json:"description"`
	Options     map[string]any `json:"options"`
}

type table struct {
	Id             string  `json:"id"`
	PrimaryFieldId string  `json:"primaryFieldId"`
	Name           string  `json:"name"`
	Description    string  `json:"description"`
	Fields         []field `json:"fields"`
}

func (b base) tables() []table {
	var reply struct {
		Tables []table `json:"tables"`
	}

	// see: https://airtable.com/developers/web/api/get-base-schema
	airtable("GET", fmt.Sprintf("/meta/bases/%s/tables", b.Id), nil, nil, &reply)
	return reply.Tables
}
