package main

import (
	"fmt"
	"net/url"
	"path/filepath"
	"strings"
)

// cast the value into the type
func cast[T any](hf *htmlFile, v any) T {
	casted, ok := v.(T)
	if !ok && !hf.castFailed {
		// write the first error since the last reset
		hf.castFailed = true
		hf.write(`<span class="error">expected <tt>%T</tt> but got <tt>%T</tt> for <pre>%v</pre></li>`, casted, v, v)
	}
	return casted
}

func (hf *htmlFile) loadTable(baseId string, t table) (records int) {
	skipDl := *downloadThreads == 0
	dl := newDownloader()
	defer func() {
		close(dl.queue)
		dl.Wait()
	}()

	field := func(f field, val any) bool {
		hf.castFailed = false // reset
		switch f.Type {
		case "checkbox":
			switch strings.ToLower(fmt.Sprint(val)) {
			case "true", "t", "yes", "y":
				hf.write(`Yes`)
			default:
				hf.write(`No`)
			}

		case "multilineText", "richText":
			hf.write(`<p class="multiline">%v</p>`, val)

		case "multipleAttachments":
			atts := cast[[]any](hf, val)
			if hf.castFailed {
				return false
			}

			// show each attachment
			hf.write(`<ul>`)
			for _, attval := range atts {
				att := cast[map[string]any](hf, attval)
				if hf.castFailed {
					continue
				}

				id := cast[string](hf, att["id"])
				filename := cast[string](hf, att["filename"])
				size := cast[float64](hf, att["size"])
				typ := cast[string](hf, att["type"])
				url := cast[string](hf, att["url"])
				if hf.castFailed {
					continue
				}
				if skipDl {
					hf.write(`<li>%s (%s, %dB, skipped)</li>`, filename, typ, int(size))
					continue
				}

				// save as id.ext on the same dir, queue download
				fnInDir := id + filepath.Ext(filename)
				fnFromRoot := filepath.Join(hf.dir, fnInDir)
				dl.push(fnFromRoot, url)
				hf.write(`<li><a href="%s">%s</a> (%s, %dB)</li>`, fnInDir, filename, typ, int(size))

			}
			hf.write(`</ul>`)

		case "multipleRecordLinks":
			tblId := cast[string](hf, f.Options["linkedTableId"])
			if hf.castFailed {
				return false
			}

			ids := cast[[]any](hf, val)
			if hf.castFailed {
				return false
			}
			hf.write(`<ul>`)
			for _, idval := range ids {
				id := cast[string](hf, idval)
				if hf.castFailed {
					break
				}

				hf.write(`<li><a href="%s">%s</a></li>`,
					fmt.Sprintf("../%s/index.html#%s", tblId, id),
					id,
				)
			}
			hf.write(`</ul>`)

		case "multipleSelects":
			vals := cast[[]any](hf, val)
			if hf.castFailed {
				return false
			}
			hf.write(`<ul>`)
			for _, v := range vals {
				hf.write(`<li>%v</li>`, v)
			}
			hf.write(`</ul>`)

		case "phoneNumber":
			hf.write(`<a href="tel:%s">%v</a>`, val, val)
		case "email":
			hf.write(`<a href="mailto:%s">%v</a>`, val, val)
		case "url":
			hf.write(`<a href="%s">%v</a>`, val, val)
		case "singleLineText", "singleSelect", "date", "dateTime", "percent",
			"number", "autoNumber", "createdTime", "currency", "formula":
			hf.write(`%v`, val)

		default:
			// TO-DO: handle each of the following types
			// case "url" :
			// case "singleCollaborator" :
			// case "multipleCollaborators" :
			// case "rollup" :
			// case "count" :
			// case "lookup" :
			// case "multipleLookupValues" :
			// case "barcode" :
			// case "rating" :
			// case "duration" :
			// case "lastModifiedTime" :
			// case "button" :
			// case "createdBy" :
			// case "lastModifiedBy" :
			// case "externalSyncSource":
			hf.write(`<tt>%s</tt><pre>%v</pre>`, f.Type, val)
		}
		return true
	}

	// load records
	dir := fmt.Sprintf("%s/%s", baseId, t.Id)
	args := url.Values{}
	args.Set("returnFieldsByFieldId", "true")
	args.Set("userLocale", *userLocale)

	for {
		var reply struct {
			Records []struct {
				Id     string         `json:"id"`
				Fields map[string]any `json:"fields"`
			} `json:"records"`
			Offset string `json:"offset"`
		}

		// see: https://airtable.com/developers/web/api/list-records
		airtable("GET", dir, args, nil, &reply)

		// read each record
		for _, r := range reply.Records {
			records++
			hf.write(`<article id="%s"><div class="id">%s</div>`, r.Id, r.Id)
			hf.write(`<h2>%v</h2>`, r.Fields[t.PrimaryFieldId])

			// write all fields
			for _, f := range t.Fields {
				val, found := r.Fields[f.Id]
				if !found {
					continue // skip unused
				}
				if s, ok := val.(string); ok && s == "" {
					continue // skip empty fields
				}
				if f.Description != "" {
					hf.write(`<label alt="%s">%s:</label> `, f.Description, f.Name)
				} else {
					hf.write(`<label>%s:</label> `, f.Name)
				}
				if !field(f, val) {
					hf.write(`<code>bad value (%s)</code><pre>%v</pre>`, f.Type, val)
				}
				hf.write("\n")
			}

			hf.write(`</article>` + "\n")
		}

		if reply.Offset == "" {
			// no more pages
			break
		}

		// fetch next page using this offset
		args.Set("offset", reply.Offset)
	}

	return
}
