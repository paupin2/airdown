package main

import (
	_ "embed"
	"errors"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var (
	//go:embed main.css
	cssContent string

	//go:embed README.md
	readmeText string

	// arguments
	outDir          = flag.String("dir", "backup.NOW", `Directory to backup data to. The text "NOW" is replaced with a timestamp`)
	tokenFilename   = flag.String("tokenfn", "token.txt", `Path to the filename that contains the token`)
	forcedToken     = flag.String("token", "", `Manually specified token`)
	logRequests     = flag.Bool("logreq", false, "Log requests")
	logResponses    = flag.Bool("logresp", false, "Log responses")
	logHeaders      = flag.Bool("loghead", false, "Log headers")
	delay           = flag.Duration("delay", time.Second/5, "Delay between requests")
	baseURL         = flag.String("baseurl", "https://api.airtable.com/v0", "Base URL for Airtable api")
	downloadThreads = flag.Int("threads", 4, "Simultaneous file downloads; set to 0 to skip downloads")
	userLocale      = flag.String("locale", "pt", "User locale for Airtable")
)

const (
	tokenEnvName = "AIRTABLE_TOKEN"
	indexhtml    = "index.html"
)

func main() {
	parseArgs()
	switch action := flag.Arg(0); action {
	case "backup":
		// see: https://github.com/rs/zerolog#pretty-logging
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
		loadToken()
		backup()

	default:
		usage("bad/no action")
	}
}

func usage(msg string, a ...any) {
	fmt.Println(readmeText)
	fmt.Println("Arguments:")
	flag.PrintDefaults()

	if len(a) > 0 {
		msg = fmt.Sprintf(msg, a...)
	}
	if msg != "" {
		fmt.Fprintf(os.Stderr, "%s\n", msg)
	}
	os.Exit(1)
}

func parseArgs() {
	flag.Parse()
	if *outDir == "" {
		usage("no/bad dir")
	} else if *tokenFilename == "" {
		usage("no/bad config")
	} else if n := *downloadThreads; n < 0 || n > 20 {
		usage("bad number of threads: %d", n)
	}
}

// loadToken tries to find set "personalToken" from:
// 1. "-token" argument
// 2. "AIRTABLE_TOKEN" environment variable
// 3. content of "-tokenfn" file
func loadToken() {
	if t := *forcedToken; t != "" {
		personalToken = t
		return
	}

	if t := os.Getenv(tokenEnvName); t != "" {
		personalToken = t
		return
	}

	buf, err := os.ReadFile(*tokenFilename)
	if err != nil {
		log.Fatal().Str("filename", *tokenFilename).Err(err).Msg("reading token")
	}
	if t := strings.TrimSpace(string(buf)); t != "" {
		personalToken = t
		return
	}

	log.Fatal().Msg("could not find token")
}

// mkdirp creates the directory (along with intermediate directories) if it doesn't exist yet
func mkdirp(parts ...string) {
	mk := func(dirname string) {
		if _, err := os.Stat(dirname); errors.Is(err, os.ErrNotExist) {
			log := log.With().Str("dir", dirname).Logger()
			if err := os.Mkdir(dirname, os.ModePerm); err != nil {
				log.Fatal().Err(err).Msg("creating dir")
			}
			log.Info().Msg("created")
		}
	}

	// join parts, clean, split again
	parts = strings.Split(filepath.Clean(filepath.Join(parts...)), string(os.PathSeparator))

	// ensure each part of the
	var fullpath string
	for _, p := range parts {
		fullpath = filepath.Join(fullpath, p)
		mk(fullpath)
	}
}
